package br.com.usp.each.estacionaki.estacionamento;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.StatusVaga;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacioaki.domain.dao.AluguelDAO;
import br.com.usp.each.estacioaki.domain.dao.EstacionamentoDAO;
import br.com.usp.each.estacioaki.domain.dao.JpaUtil;
import br.com.usp.each.estacioaki.domain.dao.VagaDAO;


public class TarefasEstacionamento 
{
	public List<Estacionamento> getEstacionamentos(){
		List<Estacionamento> estacionamentos =null;
		try {
			EntityManager entityManager = null;
			entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
			estacionamentos = new EstacionamentoDAO().findAll(entityManager);
			JpaUtil.finishTransaction(entityManager);
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  estacionamentos;
	}
	
	public Estacionamento login(Estacionamento estacionamento){
		Estacionamento estacionamentos =null;
		try {
			EntityManager entityManager = null;
			entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
			estacionamentos = new EstacionamentoDAO().Login(estacionamento.getLogin(), estacionamento.getSenha(), entityManager);
			JpaUtil.finishTransaction(entityManager);
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  estacionamentos;
	}
	
	public Aluguel OcuparVaga(Vaga vaga){
		Aluguel a = null;
		try {
			EntityManager entityManager = null;
			entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
			vaga = new VagaDAO().find(vaga.getId(), entityManager);
			a = new Aluguel();
			a.setVaga(vaga);
			a.setDataHoraInicial(Calendar.getInstance());
			vaga.setStatusVaga(StatusVaga.OCUPADO);
			new VagaDAO().update(vaga, entityManager);
			new AluguelDAO().save(a, entityManager);
			
			JpaUtil.finishTransaction(entityManager);
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  a;
	}
	
	public Aluguel LiberarVaga(Vaga vaga){
		Aluguel encontrado =null;
		try {
			EntityManager entityManager = null;
			entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
			vaga = new VagaDAO().find(vaga.getId(), entityManager);
			encontrado = new AluguelDAO().findByVaga(vaga, entityManager);
			
			if(encontrado!=null && encontrado.getVaga().getStatusVaga().equals(StatusVaga.OCUPADO)){
				encontrado.setDataHoraFinal(Calendar.getInstance());
				encontrado.getVaga().setStatusVaga(StatusVaga.DISPONIVEL);
				new VagaDAO().update(encontrado.getVaga(), entityManager);
				new AluguelDAO().update(encontrado, entityManager);
				
			}
			JpaUtil.finishTransaction(entityManager);
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  encontrado;
	}

	public Estacionamento find(Estacionamento estacionamento) {
		Estacionamento encontrado =null;
		try {
			EntityManager entityManager = null;
			entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
			encontrado = new EstacionamentoDAO().find(estacionamento.getId(), entityManager);
			
			JpaUtil.finishTransaction(entityManager);
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  encontrado;
	}
	
	public Aluguel aluguelVaga(Vaga vaga){
		Aluguel encontrado =null;
		try {
			EntityManager entityManager = null;
			entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
			vaga = new VagaDAO().find(vaga.getId(), entityManager);
			encontrado = new AluguelDAO().findByVaga(vaga, entityManager);
			
			JpaUtil.finishTransaction(entityManager);
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  encontrado;
	}

	public Aluguel reservaOcupada(Aluguel aluguel) {
		Aluguel encontrado =null;
		try {
			EntityManager entityManager = null;
			entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
			encontrado = new AluguelDAO().find(aluguel.getId(), entityManager);
			if(encontrado != null && encontrado.getVaga().getStatusVaga().equals(StatusVaga.RESERVADO)){
				encontrado.getVaga().setStatusVaga(StatusVaga.OCUPADO);
				new AluguelDAO().update(encontrado, entityManager);
			}
			JpaUtil.finishTransaction(entityManager);
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  encontrado;
	}

	public Estacionamento atualiza(Estacionamento estacionamento) {
		try {
			if(estacionamento!= null && estacionamento.getVagas().size()>0){
				EntityManager entityManager = null;
				entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
				Estacionamento atual = new EstacionamentoDAO().find(estacionamento.getId(), entityManager);
				
				if(atual.getVagas().size()<estacionamento.getVagas().size()){
					int contador =atual.getVagas().size(); 
					for(Vaga vagas : estacionamento.getVagas() ){
						if(contador-->0){
							new VagaDAO().update(vagas, entityManager);
						}
						else{
							new VagaDAO().save(vagas, entityManager);
						}
					}
				}
				else{
					for(Vaga vagas : estacionamento.getVagas() ){
						new VagaDAO().update(vagas, entityManager);
					}
				}
				if(estacionamento.getSenha()==null){
					atual.setCNPJ(estacionamento.getCNPJ());
					atual.setCoordenadas(estacionamento.getCoordenadas());
					atual.setIE(estacionamento.getIE());
					atual.setLogin(estacionamento.getLogin());
					atual.setNome(estacionamento.getNome());
					atual.setValor1hora(estacionamento.getValor1hora());
					atual.setValorDemaisHoras(estacionamento.getValorDemaisHoras());
					atual.setVagas(estacionamento.getVagas());
					new EstacionamentoDAO().update(atual, entityManager);
					JpaUtil.finishTransaction(entityManager);
					return estacionamento;
					
				}
				else{
					new EstacionamentoDAO().update(estacionamento, entityManager);
					JpaUtil.finishTransaction(entityManager);
					return estacionamento;
				}
			}
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  null;
	}
	
	public Estacionamento cadastrar(Estacionamento estacionamento) {
		try {
			if(estacionamento!= null && estacionamento.getVagas().size()>0){
				EntityManager entityManager = null;
				entityManager = JpaUtil.startTransaction(entityManager=JpaUtil.getManager());
				for(Vaga vagas : estacionamento.getVagas() ){
					new VagaDAO().save(vagas, entityManager);
				}
				new EstacionamentoDAO().save(estacionamento, entityManager);
				JpaUtil.finishTransaction(entityManager);
				
				return estacionamento;
			}
		} catch (Exception e) {
			e.getStackTrace();
			return  null;
		}
		return  null;
	}
}
